﻿namespace TU88_Mon
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
			System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
			System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
			System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
			System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
			System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
			System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.comboBoxCom = new System.Windows.Forms.ComboBox();
			this.buttonCom = new System.Windows.Forms.Button();
			this.buttonComOpen = new System.Windows.Forms.Button();
			this.buttonComClose = new System.Windows.Forms.Button();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.textBoxRoicOffset3 = new System.Windows.Forms.TextBox();
			this.textBoxRoicOffset2 = new System.Windows.Forms.TextBox();
			this.label11 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.textBoxRoicOffset0 = new System.Windows.Forms.TextBox();
			this.textBoxRoicOffset1 = new System.Windows.Forms.TextBox();
			this.checkBox3 = new System.Windows.Forms.CheckBox();
			this.checkBox2 = new System.Windows.Forms.CheckBox();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.checkBox0 = new System.Windows.Forms.CheckBox();
			this.buttonChartClear = new System.Windows.Forms.Button();
			this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
			this.tabPage4 = new System.Windows.Forms.TabPage();
			this.label12 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.textBoxTempOffset0 = new System.Windows.Forms.TextBox();
			this.textBoxTempOffset3 = new System.Windows.Forms.TextBox();
			this.textBoxTempOffset2 = new System.Windows.Forms.TextBox();
			this.textBoxTempOffset1 = new System.Windows.Forms.TextBox();
			this.checkBoxTemp3 = new System.Windows.Forms.CheckBox();
			this.checkBoxTemp2 = new System.Windows.Forms.CheckBox();
			this.checkBoxTemp1 = new System.Windows.Forms.CheckBox();
			this.checkBoxTemp0 = new System.Windows.Forms.CheckBox();
			this.button1 = new System.Windows.Forms.Button();
			this.chart3 = new System.Windows.Forms.DataVisualization.Charting.Chart();
			this.tabPage3 = new System.Windows.Forms.TabPage();
			this.checkBoxAdcGraph = new System.Windows.Forms.CheckBox();
			this.buttonClear2 = new System.Windows.Forms.Button();
			this.checkBoxAdc3 = new System.Windows.Forms.CheckBox();
			this.checkBoxAdc2 = new System.Windows.Forms.CheckBox();
			this.checkBoxAdc1 = new System.Windows.Forms.CheckBox();
			this.checkBoxAdc0 = new System.Windows.Forms.CheckBox();
			this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.buttonLogClear = new System.Windows.Forms.Button();
			this.richTextBoxLog1 = new System.Windows.Forms.RichTextBox();
			this.richTextBoxLog2 = new System.Windows.Forms.RichTextBox();
			this.tabPage5 = new System.Windows.Forms.TabPage();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.button3 = new System.Windows.Forms.Button();
			this.label16 = new System.Windows.Forms.Label();
			this.textBoxAvgParam = new System.Windows.Forms.TextBox();
			this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.textBoxReg2 = new System.Windows.Forms.TextBox();
			this.textBoxReg1 = new System.Windows.Forms.TextBox();
			this.textBoxRegAddr = new System.Windows.Forms.TextBox();
			this.buttonRegWrite = new System.Windows.Forms.Button();
			this.buttonCliSend = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.button2 = new System.Windows.Forms.Button();
			this.comboBoxNum = new System.Windows.Forms.ComboBox();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.pictureBoxOnOff = new System.Windows.Forms.PictureBox();
			this.panelOnOff = new System.Windows.Forms.Panel();
			this.button4 = new System.Windows.Forms.Button();
			this.label17 = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.button5 = new System.Windows.Forms.Button();
			this.label18 = new System.Windows.Forms.Label();
			this.button6 = new System.Windows.Forms.Button();
			this.textBoxSensTh = new System.Windows.Forms.TextBox();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
			this.tabPage4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chart3)).BeginInit();
			this.tabPage3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
			this.tabPage2.SuspendLayout();
			this.tabPage5.SuspendLayout();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxOnOff)).BeginInit();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// comboBoxCom
			// 
			this.comboBoxCom.FormattingEnabled = true;
			this.comboBoxCom.Location = new System.Drawing.Point(13, 11);
			this.comboBoxCom.Name = "comboBoxCom";
			this.comboBoxCom.Size = new System.Drawing.Size(88, 20);
			this.comboBoxCom.TabIndex = 0;
			// 
			// buttonCom
			// 
			this.buttonCom.Location = new System.Drawing.Point(107, 9);
			this.buttonCom.Name = "buttonCom";
			this.buttonCom.Size = new System.Drawing.Size(59, 23);
			this.buttonCom.TabIndex = 1;
			this.buttonCom.Text = "COM";
			this.buttonCom.UseVisualStyleBackColor = true;
			this.buttonCom.Click += new System.EventHandler(this.buttonCom_Click);
			// 
			// buttonComOpen
			// 
			this.buttonComOpen.Location = new System.Drawing.Point(197, 9);
			this.buttonComOpen.Name = "buttonComOpen";
			this.buttonComOpen.Size = new System.Drawing.Size(75, 23);
			this.buttonComOpen.TabIndex = 1;
			this.buttonComOpen.Text = "Open";
			this.buttonComOpen.UseVisualStyleBackColor = true;
			this.buttonComOpen.Click += new System.EventHandler(this.buttonComOpen_Click);
			// 
			// buttonComClose
			// 
			this.buttonComClose.Location = new System.Drawing.Point(278, 9);
			this.buttonComClose.Name = "buttonComClose";
			this.buttonComClose.Size = new System.Drawing.Size(75, 23);
			this.buttonComClose.TabIndex = 1;
			this.buttonComClose.Text = "Close";
			this.buttonComClose.UseVisualStyleBackColor = true;
			this.buttonComClose.Click += new System.EventHandler(this.buttonComClose_Click);
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage4);
			this.tabControl1.Controls.Add(this.tabPage3);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Controls.Add(this.tabPage5);
			this.tabControl1.Location = new System.Drawing.Point(13, 82);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(1035, 473);
			this.tabControl1.TabIndex = 2;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.panelOnOff);
			this.tabPage1.Controls.Add(this.textBoxRoicOffset3);
			this.tabPage1.Controls.Add(this.textBoxRoicOffset2);
			this.tabPage1.Controls.Add(this.label11);
			this.tabPage1.Controls.Add(this.label10);
			this.tabPage1.Controls.Add(this.label9);
			this.tabPage1.Controls.Add(this.label8);
			this.tabPage1.Controls.Add(this.textBoxRoicOffset0);
			this.tabPage1.Controls.Add(this.textBoxRoicOffset1);
			this.tabPage1.Controls.Add(this.checkBox3);
			this.tabPage1.Controls.Add(this.checkBox2);
			this.tabPage1.Controls.Add(this.checkBox1);
			this.tabPage1.Controls.Add(this.checkBox0);
			this.tabPage1.Controls.Add(this.buttonChartClear);
			this.tabPage1.Controls.Add(this.chart1);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(1027, 447);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Graph (ROIC)";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// textBoxRoicOffset3
			// 
			this.textBoxRoicOffset3.Location = new System.Drawing.Point(938, 299);
			this.textBoxRoicOffset3.Name = "textBoxRoicOffset3";
			this.textBoxRoicOffset3.Size = new System.Drawing.Size(73, 21);
			this.textBoxRoicOffset3.TabIndex = 3;
			this.textBoxRoicOffset3.Text = "0";
			this.textBoxRoicOffset3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// textBoxRoicOffset2
			// 
			this.textBoxRoicOffset2.Location = new System.Drawing.Point(938, 272);
			this.textBoxRoicOffset2.Name = "textBoxRoicOffset2";
			this.textBoxRoicOffset2.Size = new System.Drawing.Size(73, 21);
			this.textBoxRoicOffset2.TabIndex = 3;
			this.textBoxRoicOffset2.Text = "0";
			this.textBoxRoicOffset2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(885, 302);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(47, 12);
			this.label11.TabIndex = 15;
			this.label11.Text = "Offset 3";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(885, 275);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(47, 12);
			this.label10.TabIndex = 15;
			this.label10.Text = "Offset 2";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(885, 248);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(47, 12);
			this.label9.TabIndex = 15;
			this.label9.Text = "Offset 1";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(885, 221);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(47, 12);
			this.label8.TabIndex = 15;
			this.label8.Text = "Offset 0";
			// 
			// textBoxRoicOffset0
			// 
			this.textBoxRoicOffset0.Location = new System.Drawing.Point(938, 218);
			this.textBoxRoicOffset0.Name = "textBoxRoicOffset0";
			this.textBoxRoicOffset0.Size = new System.Drawing.Size(73, 21);
			this.textBoxRoicOffset0.TabIndex = 3;
			this.textBoxRoicOffset0.Text = "0";
			this.textBoxRoicOffset0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// textBoxRoicOffset1
			// 
			this.textBoxRoicOffset1.Location = new System.Drawing.Point(938, 245);
			this.textBoxRoicOffset1.Name = "textBoxRoicOffset1";
			this.textBoxRoicOffset1.Size = new System.Drawing.Size(73, 21);
			this.textBoxRoicOffset1.TabIndex = 3;
			this.textBoxRoicOffset1.Text = "0";
			this.textBoxRoicOffset1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// checkBox3
			// 
			this.checkBox3.AutoSize = true;
			this.checkBox3.Checked = true;
			this.checkBox3.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBox3.Location = new System.Drawing.Point(947, 396);
			this.checkBox3.Name = "checkBox3";
			this.checkBox3.Size = new System.Drawing.Size(64, 16);
			this.checkBox3.TabIndex = 2;
			this.checkBox3.Text = "TU88_3";
			this.checkBox3.UseVisualStyleBackColor = true;
			// 
			// checkBox2
			// 
			this.checkBox2.AutoSize = true;
			this.checkBox2.Checked = true;
			this.checkBox2.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBox2.Location = new System.Drawing.Point(947, 374);
			this.checkBox2.Name = "checkBox2";
			this.checkBox2.Size = new System.Drawing.Size(64, 16);
			this.checkBox2.TabIndex = 2;
			this.checkBox2.Text = "TU88_2";
			this.checkBox2.UseVisualStyleBackColor = true;
			// 
			// checkBox1
			// 
			this.checkBox1.AutoSize = true;
			this.checkBox1.Checked = true;
			this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBox1.Location = new System.Drawing.Point(947, 352);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new System.Drawing.Size(64, 16);
			this.checkBox1.TabIndex = 2;
			this.checkBox1.Text = "TU88_1";
			this.checkBox1.UseVisualStyleBackColor = true;
			// 
			// checkBox0
			// 
			this.checkBox0.AutoSize = true;
			this.checkBox0.Checked = true;
			this.checkBox0.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBox0.Location = new System.Drawing.Point(947, 330);
			this.checkBox0.Name = "checkBox0";
			this.checkBox0.Size = new System.Drawing.Size(64, 16);
			this.checkBox0.TabIndex = 2;
			this.checkBox0.Text = "TU88_0";
			this.checkBox0.UseVisualStyleBackColor = true;
			// 
			// buttonChartClear
			// 
			this.buttonChartClear.Location = new System.Drawing.Point(962, 417);
			this.buttonChartClear.Name = "buttonChartClear";
			this.buttonChartClear.Size = new System.Drawing.Size(59, 23);
			this.buttonChartClear.TabIndex = 1;
			this.buttonChartClear.Text = "Clear";
			this.buttonChartClear.UseVisualStyleBackColor = true;
			this.buttonChartClear.Click += new System.EventHandler(this.buttonChartClear_Click);
			// 
			// chart1
			// 
			chartArea1.AxisY.IsStartedFromZero = false;
			chartArea1.Name = "ChartArea1";
			this.chart1.ChartAreas.Add(chartArea1);
			legend1.Name = "Legend1";
			this.chart1.Legends.Add(legend1);
			this.chart1.Location = new System.Drawing.Point(0, 0);
			this.chart1.Name = "chart1";
			series1.ChartArea = "ChartArea1";
			series1.Legend = "Legend1";
			series1.Name = "Series1";
			this.chart1.Series.Add(series1);
			this.chart1.Size = new System.Drawing.Size(1024, 444);
			this.chart1.TabIndex = 0;
			this.chart1.Text = "chart1";
			// 
			// tabPage4
			// 
			this.tabPage4.Controls.Add(this.label12);
			this.tabPage4.Controls.Add(this.label13);
			this.tabPage4.Controls.Add(this.label14);
			this.tabPage4.Controls.Add(this.label15);
			this.tabPage4.Controls.Add(this.textBoxTempOffset0);
			this.tabPage4.Controls.Add(this.textBoxTempOffset3);
			this.tabPage4.Controls.Add(this.textBoxTempOffset2);
			this.tabPage4.Controls.Add(this.textBoxTempOffset1);
			this.tabPage4.Controls.Add(this.checkBoxTemp3);
			this.tabPage4.Controls.Add(this.checkBoxTemp2);
			this.tabPage4.Controls.Add(this.checkBoxTemp1);
			this.tabPage4.Controls.Add(this.checkBoxTemp0);
			this.tabPage4.Controls.Add(this.button1);
			this.tabPage4.Controls.Add(this.chart3);
			this.tabPage4.Location = new System.Drawing.Point(4, 22);
			this.tabPage4.Name = "tabPage4";
			this.tabPage4.Size = new System.Drawing.Size(1027, 447);
			this.tabPage4.TabIndex = 3;
			this.tabPage4.Text = "Graph (Temp)";
			this.tabPage4.UseVisualStyleBackColor = true;
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(887, 282);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(47, 12);
			this.label12.TabIndex = 16;
			this.label12.Text = "Offset 3";
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(887, 255);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(47, 12);
			this.label13.TabIndex = 17;
			this.label13.Text = "Offset 2";
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(887, 228);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(47, 12);
			this.label14.TabIndex = 18;
			this.label14.Text = "Offset 1";
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point(887, 201);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(47, 12);
			this.label15.TabIndex = 19;
			this.label15.Text = "Offset 0";
			// 
			// textBoxTempOffset0
			// 
			this.textBoxTempOffset0.Location = new System.Drawing.Point(940, 198);
			this.textBoxTempOffset0.Name = "textBoxTempOffset0";
			this.textBoxTempOffset0.Size = new System.Drawing.Size(72, 21);
			this.textBoxTempOffset0.TabIndex = 3;
			this.textBoxTempOffset0.Text = "0";
			this.textBoxTempOffset0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// textBoxTempOffset3
			// 
			this.textBoxTempOffset3.Location = new System.Drawing.Point(940, 279);
			this.textBoxTempOffset3.Name = "textBoxTempOffset3";
			this.textBoxTempOffset3.Size = new System.Drawing.Size(72, 21);
			this.textBoxTempOffset3.TabIndex = 3;
			this.textBoxTempOffset3.Text = "0";
			this.textBoxTempOffset3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// textBoxTempOffset2
			// 
			this.textBoxTempOffset2.Location = new System.Drawing.Point(940, 252);
			this.textBoxTempOffset2.Name = "textBoxTempOffset2";
			this.textBoxTempOffset2.Size = new System.Drawing.Size(72, 21);
			this.textBoxTempOffset2.TabIndex = 3;
			this.textBoxTempOffset2.Text = "0";
			this.textBoxTempOffset2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// textBoxTempOffset1
			// 
			this.textBoxTempOffset1.Location = new System.Drawing.Point(940, 225);
			this.textBoxTempOffset1.Name = "textBoxTempOffset1";
			this.textBoxTempOffset1.Size = new System.Drawing.Size(72, 21);
			this.textBoxTempOffset1.TabIndex = 3;
			this.textBoxTempOffset1.Text = "0";
			this.textBoxTempOffset1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// checkBoxTemp3
			// 
			this.checkBoxTemp3.AutoSize = true;
			this.checkBoxTemp3.Checked = true;
			this.checkBoxTemp3.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxTemp3.Location = new System.Drawing.Point(949, 386);
			this.checkBoxTemp3.Name = "checkBoxTemp3";
			this.checkBoxTemp3.Size = new System.Drawing.Size(63, 16);
			this.checkBoxTemp3.TabIndex = 2;
			this.checkBoxTemp3.Text = "Temp3";
			this.checkBoxTemp3.UseVisualStyleBackColor = true;
			// 
			// checkBoxTemp2
			// 
			this.checkBoxTemp2.AutoSize = true;
			this.checkBoxTemp2.Checked = true;
			this.checkBoxTemp2.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxTemp2.Location = new System.Drawing.Point(949, 364);
			this.checkBoxTemp2.Name = "checkBoxTemp2";
			this.checkBoxTemp2.Size = new System.Drawing.Size(63, 16);
			this.checkBoxTemp2.TabIndex = 2;
			this.checkBoxTemp2.Text = "Temp2";
			this.checkBoxTemp2.UseVisualStyleBackColor = true;
			// 
			// checkBoxTemp1
			// 
			this.checkBoxTemp1.AutoSize = true;
			this.checkBoxTemp1.Checked = true;
			this.checkBoxTemp1.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxTemp1.Location = new System.Drawing.Point(949, 342);
			this.checkBoxTemp1.Name = "checkBoxTemp1";
			this.checkBoxTemp1.Size = new System.Drawing.Size(63, 16);
			this.checkBoxTemp1.TabIndex = 2;
			this.checkBoxTemp1.Text = "Temp1";
			this.checkBoxTemp1.UseVisualStyleBackColor = true;
			// 
			// checkBoxTemp0
			// 
			this.checkBoxTemp0.AutoSize = true;
			this.checkBoxTemp0.Checked = true;
			this.checkBoxTemp0.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxTemp0.Location = new System.Drawing.Point(949, 320);
			this.checkBoxTemp0.Name = "checkBoxTemp0";
			this.checkBoxTemp0.Size = new System.Drawing.Size(63, 16);
			this.checkBoxTemp0.TabIndex = 2;
			this.checkBoxTemp0.Text = "Temp0";
			this.checkBoxTemp0.UseVisualStyleBackColor = true;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(949, 421);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 1;
			this.button1.Text = "Clear";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// chart3
			// 
			chartArea2.AxisY.IsStartedFromZero = false;
			chartArea2.Name = "ChartArea1";
			this.chart3.ChartAreas.Add(chartArea2);
			legend2.Name = "Legend1";
			this.chart3.Legends.Add(legend2);
			this.chart3.Location = new System.Drawing.Point(1, 0);
			this.chart3.Name = "chart3";
			series2.ChartArea = "ChartArea1";
			series2.Legend = "Legend1";
			series2.Name = "Series1";
			this.chart3.Series.Add(series2);
			this.chart3.Size = new System.Drawing.Size(1023, 444);
			this.chart3.TabIndex = 0;
			this.chart3.Text = "chart3";
			// 
			// tabPage3
			// 
			this.tabPage3.Controls.Add(this.checkBoxAdcGraph);
			this.tabPage3.Controls.Add(this.buttonClear2);
			this.tabPage3.Controls.Add(this.checkBoxAdc3);
			this.tabPage3.Controls.Add(this.checkBoxAdc2);
			this.tabPage3.Controls.Add(this.checkBoxAdc1);
			this.tabPage3.Controls.Add(this.checkBoxAdc0);
			this.tabPage3.Controls.Add(this.chart2);
			this.tabPage3.Location = new System.Drawing.Point(4, 22);
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.Size = new System.Drawing.Size(1027, 447);
			this.tabPage3.TabIndex = 2;
			this.tabPage3.Text = "Graph (ADC)";
			this.tabPage3.UseVisualStyleBackColor = true;
			// 
			// checkBoxAdcGraph
			// 
			this.checkBoxAdcGraph.AutoSize = true;
			this.checkBoxAdcGraph.Location = new System.Drawing.Point(926, 187);
			this.checkBoxAdcGraph.Name = "checkBoxAdcGraph";
			this.checkBoxAdcGraph.Size = new System.Drawing.Size(87, 16);
			this.checkBoxAdcGraph.TabIndex = 3;
			this.checkBoxAdcGraph.Text = "ADC Graph";
			this.checkBoxAdcGraph.UseVisualStyleBackColor = true;
			// 
			// buttonClear2
			// 
			this.buttonClear2.Location = new System.Drawing.Point(949, 421);
			this.buttonClear2.Name = "buttonClear2";
			this.buttonClear2.Size = new System.Drawing.Size(75, 23);
			this.buttonClear2.TabIndex = 2;
			this.buttonClear2.Text = "Clear";
			this.buttonClear2.UseVisualStyleBackColor = true;
			this.buttonClear2.Click += new System.EventHandler(this.buttonClear2_Click);
			// 
			// checkBoxAdc3
			// 
			this.checkBoxAdc3.AutoSize = true;
			this.checkBoxAdc3.Checked = true;
			this.checkBoxAdc3.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxAdc3.Location = new System.Drawing.Point(958, 348);
			this.checkBoxAdc3.Name = "checkBoxAdc3";
			this.checkBoxAdc3.Size = new System.Drawing.Size(55, 16);
			this.checkBoxAdc3.TabIndex = 1;
			this.checkBoxAdc3.Text = "ADC3";
			this.checkBoxAdc3.UseVisualStyleBackColor = true;
			// 
			// checkBoxAdc2
			// 
			this.checkBoxAdc2.AutoSize = true;
			this.checkBoxAdc2.Checked = true;
			this.checkBoxAdc2.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxAdc2.Location = new System.Drawing.Point(958, 326);
			this.checkBoxAdc2.Name = "checkBoxAdc2";
			this.checkBoxAdc2.Size = new System.Drawing.Size(55, 16);
			this.checkBoxAdc2.TabIndex = 1;
			this.checkBoxAdc2.Text = "ADC2";
			this.checkBoxAdc2.UseVisualStyleBackColor = true;
			// 
			// checkBoxAdc1
			// 
			this.checkBoxAdc1.AutoSize = true;
			this.checkBoxAdc1.Checked = true;
			this.checkBoxAdc1.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxAdc1.Location = new System.Drawing.Point(958, 304);
			this.checkBoxAdc1.Name = "checkBoxAdc1";
			this.checkBoxAdc1.Size = new System.Drawing.Size(55, 16);
			this.checkBoxAdc1.TabIndex = 1;
			this.checkBoxAdc1.Text = "ADC1";
			this.checkBoxAdc1.UseVisualStyleBackColor = true;
			// 
			// checkBoxAdc0
			// 
			this.checkBoxAdc0.AutoSize = true;
			this.checkBoxAdc0.Checked = true;
			this.checkBoxAdc0.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxAdc0.Location = new System.Drawing.Point(958, 282);
			this.checkBoxAdc0.Name = "checkBoxAdc0";
			this.checkBoxAdc0.Size = new System.Drawing.Size(55, 16);
			this.checkBoxAdc0.TabIndex = 1;
			this.checkBoxAdc0.Text = "ADC0";
			this.checkBoxAdc0.UseVisualStyleBackColor = true;
			// 
			// chart2
			// 
			chartArea3.AxisY.IsStartedFromZero = false;
			chartArea3.Name = "ChartArea1";
			this.chart2.ChartAreas.Add(chartArea3);
			legend3.Name = "Legend1";
			this.chart2.Legends.Add(legend3);
			this.chart2.Location = new System.Drawing.Point(4, 4);
			this.chart2.Name = "chart2";
			series3.ChartArea = "ChartArea1";
			series3.Legend = "Legend1";
			series3.Name = "Series1";
			this.chart2.Series.Add(series3);
			this.chart2.Size = new System.Drawing.Size(1020, 440);
			this.chart2.TabIndex = 0;
			this.chart2.Text = "chart2";
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.buttonLogClear);
			this.tabPage2.Controls.Add(this.richTextBoxLog1);
			this.tabPage2.Controls.Add(this.richTextBoxLog2);
			this.tabPage2.Controls.Add(this.pictureBoxOnOff);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(1027, 447);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "LOG";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// buttonLogClear
			// 
			this.buttonLogClear.Location = new System.Drawing.Point(946, 417);
			this.buttonLogClear.Name = "buttonLogClear";
			this.buttonLogClear.Size = new System.Drawing.Size(75, 23);
			this.buttonLogClear.TabIndex = 1;
			this.buttonLogClear.Text = "Clear";
			this.buttonLogClear.UseVisualStyleBackColor = true;
			this.buttonLogClear.Click += new System.EventHandler(this.buttonLogClear_Click);
			// 
			// richTextBoxLog1
			// 
			this.richTextBoxLog1.Location = new System.Drawing.Point(7, 7);
			this.richTextBoxLog1.Name = "richTextBoxLog1";
			this.richTextBoxLog1.Size = new System.Drawing.Size(1014, 433);
			this.richTextBoxLog1.TabIndex = 0;
			this.richTextBoxLog1.Text = "";
			// 
			// richTextBoxLog2
			// 
			this.richTextBoxLog2.Location = new System.Drawing.Point(500, 7);
			this.richTextBoxLog2.Name = "richTextBoxLog2";
			this.richTextBoxLog2.Size = new System.Drawing.Size(521, 54);
			this.richTextBoxLog2.TabIndex = 3;
			this.richTextBoxLog2.Text = "";
			// 
			// tabPage5
			// 
			this.tabPage5.Controls.Add(this.groupBox1);
			this.tabPage5.Location = new System.Drawing.Point(4, 22);
			this.tabPage5.Name = "tabPage5";
			this.tabPage5.Size = new System.Drawing.Size(1027, 447);
			this.tabPage5.TabIndex = 4;
			this.tabPage5.Text = "Config";
			this.tabPage5.UseVisualStyleBackColor = true;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.textBoxSensTh);
			this.groupBox1.Controls.Add(this.button6);
			this.groupBox1.Controls.Add(this.button3);
			this.groupBox1.Controls.Add(this.label18);
			this.groupBox1.Controls.Add(this.label16);
			this.groupBox1.Controls.Add(this.textBoxAvgParam);
			this.groupBox1.Location = new System.Drawing.Point(10, 10);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(245, 84);
			this.groupBox1.TabIndex = 3;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "FW Config";
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(169, 20);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(55, 23);
			this.button3.TabIndex = 0;
			this.button3.Text = "Set";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point(8, 25);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(88, 12);
			this.label16.TabIndex = 2;
			this.label16.Text = "Average Count";
			// 
			// textBoxAvgParam
			// 
			this.textBoxAvgParam.Location = new System.Drawing.Point(107, 20);
			this.textBoxAvgParam.Name = "textBoxAvgParam";
			this.textBoxAvgParam.Size = new System.Drawing.Size(45, 21);
			this.textBoxAvgParam.TabIndex = 1;
			this.textBoxAvgParam.Text = "8";
			this.textBoxAvgParam.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.textBoxAvgParam.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(222, 7);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(51, 12);
			this.label3.TabIndex = 14;
			this.label3.Text = "Reg 2nd";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(146, 7);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(47, 12);
			this.label2.TabIndex = 15;
			this.label2.Text = "Reg 1st";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(65, 7);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(57, 12);
			this.label1.TabIndex = 16;
			this.label1.Text = "Reg Addr";
			// 
			// textBoxReg2
			// 
			this.textBoxReg2.Location = new System.Drawing.Point(233, 22);
			this.textBoxReg2.Name = "textBoxReg2";
			this.textBoxReg2.Size = new System.Drawing.Size(40, 21);
			this.textBoxReg2.TabIndex = 11;
			this.textBoxReg2.Text = "3c";
			this.textBoxReg2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// textBoxReg1
			// 
			this.textBoxReg1.Location = new System.Drawing.Point(153, 22);
			this.textBoxReg1.Name = "textBoxReg1";
			this.textBoxReg1.Size = new System.Drawing.Size(40, 21);
			this.textBoxReg1.TabIndex = 12;
			this.textBoxReg1.Text = "0b";
			this.textBoxReg1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// textBoxRegAddr
			// 
			this.textBoxRegAddr.Location = new System.Drawing.Point(82, 22);
			this.textBoxRegAddr.Name = "textBoxRegAddr";
			this.textBoxRegAddr.Size = new System.Drawing.Size(40, 21);
			this.textBoxRegAddr.TabIndex = 13;
			this.textBoxRegAddr.Text = "12";
			this.textBoxRegAddr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// buttonRegWrite
			// 
			this.buttonRegWrite.Location = new System.Drawing.Point(294, 20);
			this.buttonRegWrite.Name = "buttonRegWrite";
			this.buttonRegWrite.Size = new System.Drawing.Size(59, 23);
			this.buttonRegWrite.TabIndex = 10;
			this.buttonRegWrite.Text = "Set";
			this.buttonRegWrite.UseVisualStyleBackColor = true;
			this.buttonRegWrite.Click += new System.EventHandler(this.buttonRegWrite_Click);
			// 
			// buttonCliSend
			// 
			this.buttonCliSend.Location = new System.Drawing.Point(13, 48);
			this.buttonCliSend.Name = "buttonCliSend";
			this.buttonCliSend.Size = new System.Drawing.Size(99, 23);
			this.buttonCliSend.TabIndex = 17;
			this.buttonCliSend.Text = "Mon Disable";
			this.buttonCliSend.UseVisualStyleBackColor = true;
			this.buttonCliSend.Click += new System.EventHandler(this.buttonCliSend_Click_1);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(64, 25);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(18, 12);
			this.label4.TabIndex = 15;
			this.label4.Text = "0x";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(135, 25);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(18, 12);
			this.label5.TabIndex = 15;
			this.label5.Text = "0x";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(215, 25);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(18, 12);
			this.label6.TabIndex = 15;
			this.label6.Text = "0x";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(16, 7);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(32, 12);
			this.label7.TabIndex = 16;
			this.label7.Text = "Num";
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(140, 48);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(75, 23);
			this.button2.TabIndex = 18;
			this.button2.Text = "Clear All";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// comboBoxNum
			// 
			this.comboBoxNum.FormattingEnabled = true;
			this.comboBoxNum.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "ALL"});
			this.comboBoxNum.Location = new System.Drawing.Point(9, 23);
			this.comboBoxNum.Name = "comboBoxNum";
			this.comboBoxNum.Size = new System.Drawing.Size(48, 20);
			this.comboBoxNum.TabIndex = 20;
			this.comboBoxNum.Text = "ALL";
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(832, 9);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(212, 82);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pictureBox1.TabIndex = 19;
			this.pictureBox1.TabStop = false;
			// 
			// pictureBoxOnOff
			// 
			this.pictureBoxOnOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.pictureBoxOnOff.Image = global::TU88_Mon.Properties.Resources.LightOff1;
			this.pictureBoxOnOff.Location = new System.Drawing.Point(863, 19);
			this.pictureBoxOnOff.Name = "pictureBoxOnOff";
			this.pictureBoxOnOff.Size = new System.Drawing.Size(134, 90);
			this.pictureBoxOnOff.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBoxOnOff.TabIndex = 16;
			this.pictureBoxOnOff.TabStop = false;
			// 
			// panelOnOff
			// 
			this.panelOnOff.BackgroundImage = global::TU88_Mon.Properties.Resources.LightOff1;
			this.panelOnOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.panelOnOff.Location = new System.Drawing.Point(911, 98);
			this.panelOnOff.Name = "panelOnOff";
			this.panelOnOff.Size = new System.Drawing.Size(91, 114);
			this.panelOnOff.TabIndex = 16;
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(233, 5);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(62, 23);
			this.button4.TabIndex = 18;
			this.button4.Text = "Adj";
			this.button4.UseVisualStyleBackColor = true;
			this.button4.Click += new System.EventHandler(this.button4_Click_1);
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Location = new System.Drawing.Point(8, 10);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(76, 12);
			this.label17.TabIndex = 2;
			this.label17.Text = "Offset Adjust";
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.textBoxReg2);
			this.panel1.Controls.Add(this.comboBoxNum);
			this.panel1.Controls.Add(this.label7);
			this.panel1.Controls.Add(this.textBoxRegAddr);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.buttonRegWrite);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Controls.Add(this.label6);
			this.panel1.Controls.Add(this.textBoxReg1);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.label5);
			this.panel1.Location = new System.Drawing.Point(442, 9);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(365, 50);
			this.panel1.TabIndex = 21;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.button5);
			this.panel2.Controls.Add(this.button4);
			this.panel2.Controls.Add(this.label17);
			this.panel2.Location = new System.Drawing.Point(442, 65);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(365, 33);
			this.panel2.TabIndex = 22;
			// 
			// button5
			// 
			this.button5.Location = new System.Drawing.Point(137, 5);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(56, 23);
			this.button5.TabIndex = 19;
			this.button5.Text = "Zero";
			this.button5.UseVisualStyleBackColor = true;
			this.button5.Click += new System.EventHandler(this.button5_Click);
			// 
			// label18
			// 
			this.label18.AutoSize = true;
			this.label18.Location = new System.Drawing.Point(8, 58);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(62, 12);
			this.label18.TabIndex = 2;
			this.label18.Text = "Threshold";
			// 
			// button6
			// 
			this.button6.Location = new System.Drawing.Point(169, 53);
			this.button6.Name = "button6";
			this.button6.Size = new System.Drawing.Size(55, 23);
			this.button6.TabIndex = 3;
			this.button6.Text = "Set";
			this.button6.UseVisualStyleBackColor = true;
			this.button6.Click += new System.EventHandler(this.button6_Click);
			// 
			// textBoxSensTh
			// 
			this.textBoxSensTh.Location = new System.Drawing.Point(107, 55);
			this.textBoxSensTh.Name = "textBoxSensTh";
			this.textBoxSensTh.Size = new System.Drawing.Size(45, 21);
			this.textBoxSensTh.TabIndex = 4;
			this.textBoxSensTh.Text = "10000";
			this.textBoxSensTh.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1060, 560);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.buttonCliSend);
			this.Controls.Add(this.tabControl1);
			this.Controls.Add(this.buttonComClose);
			this.Controls.Add(this.buttonComOpen);
			this.Controls.Add(this.buttonCom);
			this.Controls.Add(this.comboBoxCom);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "Form1";
			this.Text = "TU88_Mon";
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
			this.tabPage4.ResumeLayout(false);
			this.tabPage4.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chart3)).EndInit();
			this.tabPage3.ResumeLayout(false);
			this.tabPage3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
			this.tabPage2.ResumeLayout(false);
			this.tabPage5.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxOnOff)).EndInit();
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ComboBox comboBoxCom;
		private System.Windows.Forms.Button buttonCom;
		private System.Windows.Forms.Button buttonComOpen;
		private System.Windows.Forms.Button buttonComClose;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.RichTextBox richTextBoxLog2;
		private System.IO.Ports.SerialPort serialPort1;
		private System.Windows.Forms.RichTextBox richTextBoxLog1;
		private System.Windows.Forms.Button buttonChartClear;
		private System.Windows.Forms.Button buttonLogClear;
		private System.Windows.Forms.CheckBox checkBox3;
		private System.Windows.Forms.CheckBox checkBox2;
		private System.Windows.Forms.CheckBox checkBox1;
		private System.Windows.Forms.CheckBox checkBox0;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textBoxReg2;
		private System.Windows.Forms.TextBox textBoxReg1;
		private System.Windows.Forms.TextBox textBoxRegAddr;
		private System.Windows.Forms.Button buttonRegWrite;
		private System.Windows.Forms.Button buttonCliSend;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TabPage tabPage3;
		private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
		private System.Windows.Forms.Button buttonClear2;
		private System.Windows.Forms.CheckBox checkBoxAdc3;
		private System.Windows.Forms.CheckBox checkBoxAdc2;
		private System.Windows.Forms.CheckBox checkBoxAdc1;
		private System.Windows.Forms.CheckBox checkBoxAdc0;
		private System.Windows.Forms.TabPage tabPage4;
		private System.Windows.Forms.CheckBox checkBoxTemp3;
		private System.Windows.Forms.CheckBox checkBoxTemp2;
		private System.Windows.Forms.CheckBox checkBoxTemp1;
		private System.Windows.Forms.CheckBox checkBoxTemp0;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.DataVisualization.Charting.Chart chart3;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.TextBox textBoxRoicOffset3;
		private System.Windows.Forms.TextBox textBoxRoicOffset2;
		private System.Windows.Forms.TextBox textBoxRoicOffset0;
		private System.Windows.Forms.TextBox textBoxRoicOffset1;
		private System.Windows.Forms.TextBox textBoxTempOffset0;
		private System.Windows.Forms.TextBox textBoxTempOffset3;
		private System.Windows.Forms.TextBox textBoxTempOffset2;
		private System.Windows.Forms.TextBox textBoxTempOffset1;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.ComboBox comboBoxNum;
		private System.Windows.Forms.TabPage tabPage5;
		private System.Windows.Forms.TextBox textBoxAvgParam;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.CheckBox checkBoxAdcGraph;
		private System.Windows.Forms.PictureBox pictureBoxOnOff;
		private System.Windows.Forms.Panel panelOnOff;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.TextBox textBoxSensTh;
		private System.Windows.Forms.Button button6;
		private System.Windows.Forms.Label label18;
	}
}

