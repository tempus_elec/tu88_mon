﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace TU88_Mon
{
	public partial class Form1 : Form
	{
		public void ChartInit()
		{
			Series chartTempus0 = chart1.Series.Add("TU88_0");
			Series chartTempus1 = chart1.Series.Add("TU88_1");
			Series chartTempus2 = chart1.Series.Add("TU88_2");
			Series chartTempus3 = chart1.Series.Add("TU88_3");

			Series chartAdc0 = chart2.Series.Add("TU88_0");
			Series chartAdc1 = chart2.Series.Add("TU88_1");
			Series chartAdc2 = chart2.Series.Add("TU88_2");
			Series chartAdc3 = chart2.Series.Add("TU88_3");

			Series chartTemp0 = chart3.Series.Add("TU88_0");
			Series chartTemp1 = chart3.Series.Add("TU88_1");
			Series chartTemp2 = chart3.Series.Add("TU88_2");
			Series chartTemp3 = chart3.Series.Add("TU88_3");

			chartTempus0.ChartType = SeriesChartType.Line;
			chart1.Series["TU88_0"].Color = System.Drawing.Color.Red;

			chartTempus1.ChartType = SeriesChartType.Line;
			chart1.Series["TU88_1"].Color = System.Drawing.Color.Green;

			chartTempus2.ChartType = SeriesChartType.Line;
			chart1.Series["TU88_2"].Color = System.Drawing.Color.Blue;

			chartTempus3.ChartType = SeriesChartType.Line;
			chart1.Series["TU88_3"].Color = System.Drawing.Color.Violet;

			chartAdc0.ChartType = SeriesChartType.Line;
			chart2.Series["TU88_0"].Color = System.Drawing.Color.Red;

			chartAdc1.ChartType = SeriesChartType.Line;
			chart2.Series["TU88_1"].Color = System.Drawing.Color.Green;

			chartAdc2.ChartType = SeriesChartType.Line;
			chart2.Series["TU88_2"].Color = System.Drawing.Color.Blue;

			chartAdc3.ChartType = SeriesChartType.Line;
			chart3.Series["TU88_3"].Color = System.Drawing.Color.Violet;

			chartTemp0.ChartType = SeriesChartType.Line;
			chart3.Series["TU88_0"].Color = System.Drawing.Color.Red;

			chartTemp1.ChartType = SeriesChartType.Line;
			chart3.Series["TU88_1"].Color = System.Drawing.Color.Green;

			chartTemp2.ChartType = SeriesChartType.Line;
			chart3.Series["TU88_2"].Color = System.Drawing.Color.Blue;

			chartTemp3.ChartType = SeriesChartType.Line;
			chart3.Series["TU88_3"].Color = System.Drawing.Color.Violet;
		}

		private void chart_tu88_update(UInt64 count, string tu88_0, string tu88_1, string tu88_2, string tu88_3)
		{
			if (chart1.InvokeRequired)
			{
				chart1.Invoke(new MethodInvoker(delegate ()
				{
					try
					{
						if(checkBox0.Checked)
							chart1.Series["TU88_0"].Points.AddXY(count, Convert.ToInt32(tu88_0) + Convert.ToInt32(textBoxRoicOffset0.Text));

						if (checkBox1.Checked)
							chart1.Series["TU88_1"].Points.AddXY(count, Convert.ToInt32(tu88_1) + Convert.ToInt32(textBoxRoicOffset1.Text));

						if (checkBox2.Checked)
							chart1.Series["TU88_2"].Points.AddXY(count, Convert.ToInt32(tu88_2) + Convert.ToInt32(textBoxRoicOffset2.Text));

						if (checkBox3.Checked)
							chart1.Series["TU88_3"].Points.AddXY(count, Convert.ToInt32(tu88_3) + Convert.ToInt32(textBoxRoicOffset3.Text));

						chart1.Invalidate();
					}
					catch(Exception ex)
					{
						ERR(ex.Message);
					}
				}));
			}
			else
			{
				try
				{
					if (checkBox0.Checked)
						chart1.Series["TU88_0"].Points.AddXY(count, Convert.ToInt32(tu88_0) + Convert.ToInt32(textBoxRoicOffset0.Text));

					if (checkBox1.Checked)
						chart1.Series["TU88_1"].Points.AddXY(count, Convert.ToInt32(tu88_1) + Convert.ToInt32(textBoxRoicOffset1.Text));

					if (checkBox2.Checked)
						chart1.Series["TU88_2"].Points.AddXY(count, Convert.ToInt32(tu88_2) + Convert.ToInt32(textBoxRoicOffset2.Text));

					if (checkBox3.Checked)
						chart1.Series["TU88_3"].Points.AddXY(count, Convert.ToInt32(tu88_3) + Convert.ToInt32(textBoxRoicOffset3.Text));

					chart1.Invalidate();
				}
				catch (Exception ex)
				{
					ERR(ex.Message);
				}
			}
		}

		private void chart_tu88_adc_update(UInt64 count, string tu88_0, string tu88_1, string tu88_2, string tu88_3)
		{
			if (checkBoxAdcGraph.Checked == false)
				return;

			if (chart2.InvokeRequired)
			{
				chart2.Invoke(new MethodInvoker(delegate ()
				{
					try
					{
						if (checkBoxAdc0.Checked)
							chart2.Series["TU88_0"].Points.AddXY(count, Convert.ToUInt32(tu88_0));

						if (checkBoxAdc1.Checked)
							chart2.Series["TU88_1"].Points.AddXY(count, Convert.ToDouble(tu88_1));

						if (checkBoxAdc2.Checked)
							chart2.Series["TU88_2"].Points.AddXY(count, Convert.ToDouble(tu88_2));

						if (checkBoxAdc3.Checked)
							chart2.Series["TU88_3"].Points.AddXY(count, Convert.ToDouble(tu88_3));

						chart2.Invalidate();
					}
					catch (Exception ex)
					{
						ERR(ex.Message);
					}
				}));
			}
			else
			{
				try
				{
					if (checkBoxAdc0.Checked)
						chart2.Series["TU88_0"].Points.AddXY(count, Convert.ToDouble(tu88_0));

					if (checkBoxAdc1.Checked)
						chart2.Series["TU88_1"].Points.AddXY(count, Convert.ToDouble(tu88_1));

					if (checkBoxAdc2.Checked)
						chart2.Series["TU88_2"].Points.AddXY(count, Convert.ToDouble(tu88_2));

					if (checkBoxAdc3.Checked)
						chart2.Series["TU88_3"].Points.AddXY(count, Convert.ToDouble(tu88_3));

					chart2.Invalidate();
				}
				catch (Exception ex)
				{
					ERR(ex.Message);
				}
			}
		}

		private void chart_tu88_temp_update(UInt64 count, string tu88_0, string tu88_1, string tu88_2, string tu88_3)
		{
			if (chart3.InvokeRequired)
			{
				chart3.Invoke(new MethodInvoker(delegate ()
				{
					try
					{
						if (checkBoxTemp0.Checked)
							chart3.Series["TU88_0"].Points.AddXY(count, Convert.ToDouble(tu88_0) + Convert.ToInt32(textBoxTempOffset0.Text));

						if (checkBoxTemp1.Checked)
							chart3.Series["TU88_1"].Points.AddXY(count, Convert.ToDouble(tu88_1) + Convert.ToInt32(textBoxTempOffset0.Text));

						if (checkBoxTemp2.Checked)
							chart3.Series["TU88_2"].Points.AddXY(count, Convert.ToDouble(tu88_2) + Convert.ToInt32(textBoxTempOffset0.Text));

						if (checkBoxTemp3.Checked)
							chart3.Series["TU88_3"].Points.AddXY(count, Convert.ToDouble(tu88_3) + Convert.ToInt32(textBoxTempOffset0.Text));

						chart3.Invalidate();
					}
					catch (Exception ex)
					{
						ERR(ex.Message);
					}
				}));
			}
			else
			{
				try
				{
					if (checkBoxTemp0.Checked)
						chart3.Series["TU88_0"].Points.AddXY(count, Convert.ToDouble(tu88_0));

					if (checkBoxTemp1.Checked)
						chart3.Series["TU88_1"].Points.AddXY(count, Convert.ToDouble(tu88_1));

					if (checkBoxTemp2.Checked)
						chart3.Series["TU88_2"].Points.AddXY(count, Convert.ToDouble(tu88_2));

					if (checkBoxTemp3.Checked)
						chart3.Series["TU88_3"].Points.AddXY(count, Convert.ToDouble(tu88_3));

					chart3.Invalidate();
				}
				catch (Exception ex)
				{
					ERR(ex.Message);
				}
			}
		}

		private void ChartClear()
		{
			foreach (var series in chart1.Series)
			{
				series.Points.Clear();
			}
			counter = 0;
		}

		private void ChartClear2()
		{
			foreach (var series in chart2.Series)
			{
				series.Points.Clear();
			}
			adcCounter = 0;
		}

		private void ChartClear3()
		{
			foreach (var series in chart3.Series)
			{
				series.Points.Clear();
			}
			tempCounter = 0;
		}
	}
}
