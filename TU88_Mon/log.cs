﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.CompilerServices;
using System.IO;

namespace TU88_Mon
{
	public partial class Form1 : Form
	{
		// log 저장을 위해 path와 file 정의 /////////////////////////////////////////////////
		FileStream file_streamLog;
		StreamWriter psWriterLog;

		FileStream file_streamCsv;
		StreamWriter psWriterCsv;

		public string desktop_path;
		DateTime now;		
		////////////////////////////////////////////////////////////////////////////////////

		public void LogInit()
		{
			file_streamLog = null;
			psWriterLog = null;

			file_streamCsv = null;
			psWriterCsv = null;

			now = DateTime.Now;
			desktop_path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
			desktop_path = desktop_path + "/TU88_Mon_" + now.ToString("yyyy-MM-dd-H-mm-ss");
			DirectoryInfo di = new DirectoryInfo(desktop_path);
			if (di.Exists == false)
			{
				di.Create();
				OpenLogFiles();
			}
			else
			{
				MessageBox.Show("Log folder creation failed");
			}
		}

		public void OpenLogFiles()
		{
			string path;

			try
			{
				now = DateTime.Now;

				if (file_streamLog == null)
				{
					// Default Log file
					path = desktop_path + "/log" + ".log";
					file_streamLog = new FileStream(path, FileMode.Create, FileAccess.Write);
					psWriterLog = new StreamWriter(file_streamLog, System.Text.Encoding.Default);
					psWriterLog.Write("CO2 Default Log File\n");
				}
				else
				{
					MessageBox.Show("Log file creation has problem...\n");
				}

				if (file_streamCsv == null)
				{
					// Default Log file
					path = desktop_path + "/msg" + ".csv";
					file_streamCsv = new FileStream(path, FileMode.Create, FileAccess.Write);
					psWriterCsv = new StreamWriter(file_streamCsv, System.Text.Encoding.Default);
					psWriterCsv.Write("RAW,Temp,RAW,Temp,RAW,Temp,RAW,Temp\n");
				}
				else
				{
					MessageBox.Show("Log file creation has problem...\n");
				}
			}
			catch (Exception ex)  // CS0168
			{
				MessageBox.Show(CallerName() + " : " + ex.Message);
			}
		}

		public void CloseLogFiles()
		{
			try
			{
				if (psWriterLog == null)
				{
					LOG("psWriterLog Log file not opened\n");
				}
				else
				{
					psWriterLog.Flush();
					psWriterLog.Close();
					psWriterLog = null;
				}

				if (psWriterCsv == null)
				{
					LOG("psWriterMsg csv file not opened\n");
				}
				else
				{
					psWriterCsv.Flush();
					psWriterCsv.Close();
					psWriterCsv = null;
				}
			}
			catch (Exception ex)  // CS0168
			{
				MessageBox.Show(CallerName() + " : " + ex.Message);
			}
		}

		public void _L(string str)
		{
			now = DateTime.Now;
			str = "[" + DateTime.Now.ToString("H:mm:ss") + "] " + str;
			if (richTextBoxLog1.InvokeRequired)
			{
				richTextBoxLog1.Invoke(new MethodInvoker(delegate ()
				{
					richTextBoxLog1.SelectionColor = System.Drawing.Color.Black;
					richTextBoxLog1.AppendText(str);
					richTextBoxLog1.ScrollToCaret();

					if (psWriterLog != null)
						psWriterLog.Write(str);
				}));
			}
			else
			{
				richTextBoxLog1.SelectionColor = System.Drawing.Color.Black;
				richTextBoxLog1.AppendText(str);
				richTextBoxLog1.ScrollToCaret();

				if (psWriterLog != null)
					psWriterLog.Write(str);
			}
		}

		public void LOG(string str)
		{
			_L(str);
		}

		public void ERR(string str)
		{
			//if (checkBoxErr.Checked == true)
			{
				now = DateTime.Now;
				str = "[" + DateTime.Now.ToString("H:mm:ss") + "] " + str;
				if (richTextBoxLog1.InvokeRequired)
				{
					richTextBoxLog1.Invoke(new MethodInvoker(delegate ()
					{
						richTextBoxLog1.SelectionColor = System.Drawing.Color.Red;
						richTextBoxLog1.AppendText(str);
						richTextBoxLog1.ScrollToCaret();

						if (psWriterLog != null)
							psWriterLog.Write(str);

						richTextBoxLog1.SelectionColor = System.Drawing.Color.Black;
					}));
				}
				else
				{
					richTextBoxLog1.SelectionColor = System.Drawing.Color.Red;
					richTextBoxLog1.AppendText(str);
					richTextBoxLog1.ScrollToCaret();

					if (psWriterLog != null)
						psWriterLog.Write(str);

					richTextBoxLog1.SelectionColor = System.Drawing.Color.Black;
				}
			}

		}

		public void INFO(string str)
		{
			{
				now = DateTime.Now;
				str = "[" + DateTime.Now.ToString("H:mm:ss") + "] " + str;
				if (richTextBoxLog2.InvokeRequired)
				{
					richTextBoxLog2.Invoke(new MethodInvoker(delegate ()
					{
						richTextBoxLog2.SelectionColor = System.Drawing.Color.Blue;
						richTextBoxLog2.AppendText(str);
						richTextBoxLog2.ScrollToCaret();

						if (psWriterLog != null)
							psWriterLog.Write(str);

						richTextBoxLog2.SelectionColor = System.Drawing.Color.Black;
					}));
				}
				else
				{
					richTextBoxLog2.SelectionColor = System.Drawing.Color.Blue;
					richTextBoxLog2.AppendText(str);
					richTextBoxLog2.ScrollToCaret();

					if (psWriterLog != null)
						psWriterLog.Write(str);

					richTextBoxLog2.SelectionColor = System.Drawing.Color.Black;
				}
			}
		}

		public void LOG_Clear()
		{
			richTextBoxLog1.Clear();
		}

		public string CallerName([CallerMemberName] string callerName = "")
		{
			return callerName;
		}

		private void CSV_SAVE(string str)
		{
			string[] spData = str.Split(' ');

			try
			{
				for (int i = 1; i < spData.Length; i++)
				{
					psWriterCsv.Write(spData[i] + ",");
				}
				psWriterCsv.Write("\n");
			}
			catch (Exception ex)
			{
				ERR(CallerName() + " : " + ex.Message);
			}
		}
	}
}
