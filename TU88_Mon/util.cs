﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TU88_Mon
{
	public partial class Form1 : Form
	{
		Thread t1 = null;

		public string Str2Hex(string strData)
		{
			string resultHex = string.Empty;
			byte[] arr_byteStr = Encoding.Default.GetBytes(strData);

			foreach (byte byteStr in arr_byteStr)
				resultHex += string.Format("{0:X2}", byteStr);

			return resultHex;
		}

		public DateTime Delay(int MS)
		{
			DateTime ThisMoment = DateTime.Now;
			TimeSpan duration = new TimeSpan(0, 0, 0, 0, MS);
			DateTime AfterWards = ThisMoment.Add(duration);

			while (AfterWards >= ThisMoment)
			{
				System.Windows.Forms.Application.DoEvents();
				ThisMoment = DateTime.Now;
			}

			return DateTime.Now;
		}

		public void LoadOnOffImage(bool onoff)
		{
			if (onoff)      // in case of on
			{
				//pictureBoxOnOff.Load("../../LightOn1.jpg");
				//pictureBoxOnOff.Load("Properties.Resources.LightOn1.jpg");
				panelOnOff.BackgroundImage = Properties.Resources.LightOn1;
			}
			else
			{
				//pictureBoxOnOff.Load("../../LightOff1.jpg");
				//pictureBoxOnOff.Load("Properties.Resources.LightOff1.jpg");
				panelOnOff.BackgroundImage = Properties.Resources.LightOff1;
			}
		}

		public void SetOffset()
		{
			t1 = new Thread(new ThreadStart(SetOffsetThread));
			t1.Start();
		}

		public void SetOffsetThread()
		{
			Int32 tu88_0_offset = 0;
			Int32 tu88_1_offset = 0;
			Int32 tu88_2_offset = 0;
			Int32 tu88_3_offset = 0;
			Int32 tu88_offset = 0;

			// clear current offset
			uartSendStr("tu88_offset 0 0 0 0\n");
			Delay(1000);

			if (adjEvent == null)
				adjEvent = new AutoResetEvent(false);

			adjEvent.WaitOne();
			tu88_offset = tu88_0_value;

			tu88_0_offset = tu88_offset - tu88_0_value;
			tu88_1_offset = tu88_offset - tu88_1_value;
			tu88_2_offset = tu88_offset - tu88_2_value;
			tu88_3_offset = tu88_offset - tu88_3_value;

			uartSendStr("tu88_offset " + tu88_0_offset + " " + tu88_1_offset + " " + tu88_2_offset + " " + tu88_3_offset + "\n");
			
			textBoxRoicOffset0.Text = tu88_0_offset.ToString();
			textBoxRoicOffset1.Text = tu88_1_offset.ToString();
			textBoxRoicOffset2.Text = tu88_2_offset.ToString();
			textBoxRoicOffset3.Text = tu88_3_offset.ToString();

			uartSendStr("tu88_offset 0 0 0 0\n");
			Delay(1000);
		}
	}
}
