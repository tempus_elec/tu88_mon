﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TU88_Mon
{
	public partial class Form1 : Form
	{
		bool monFlag = true;
		bool onoff = false;
		AutoResetEvent adjEvent;

		public Form1()
		{
			InitializeComponent();

			ChartInit();
			LogInit();
		}

		private void buttonCom_Click(object sender, EventArgs e)
		{
			UartCom();
		}

		private void buttonComOpen_Click(object sender, EventArgs e)
		{
			UartOpen();
		}

		private void buttonComClose_Click(object sender, EventArgs e)
		{
			UartClose();
		}

		private void buttonChartClear_Click(object sender, EventArgs e)
		{
			ChartClear();
			
			if(onoff)
			{
				onoff = false;
				LoadOnOffImage(onoff);
			}
			else
			{
				onoff = true;
				LoadOnOffImage(onoff);
			}
		}

		private void buttonLogClear_Click(object sender, EventArgs e)
		{
			LOG_Clear();
		}

		private void buttonCliSend_Click(object sender, EventArgs e)
		{

		}

		private void buttonCliSend_Click_1(object sender, EventArgs e)
		{
			if(monFlag == true)
			{
				uartSendStr("mon 0\n");
				monFlag = false;
				buttonCliSend.Text = "Mon Enable";
			}
			else
			{
				uartSendStr("mon 1\n");
				monFlag = true;
				buttonCliSend.Text = "Mon Disable";
			}
		}

		private void buttonRegWrite_Click(object sender, EventArgs e)
		{
			string adr, val1, val2, num;

			num = Convert.ToString(comboBoxNum.SelectedItem);
			if (num == "ALL")
				num = "4";

			adr = Convert.ToString(byte.Parse(textBoxRegAddr.Text, System.Globalization.NumberStyles.HexNumber));
			val1 = Convert.ToString(byte.Parse(textBoxReg1.Text, System.Globalization.NumberStyles.HexNumber));
			val2 = Convert.ToString(byte.Parse(textBoxReg2.Text, System.Globalization.NumberStyles.HexNumber));

			//uartSendStr("roic " + textBoxTu88Num.Text + " " + adr + " " + val1 + " " + val2 + "\n");
			uartSendStr("roic " + num + " " + adr + " " + val1 + " " + val2 + "\n");
		}

		private void button1_Click(object sender, EventArgs e)
		{
			ChartClear3();
		}

		private void buttonClear2_Click(object sender, EventArgs e)
		{
			ChartClear2();
		}

		private void button2_Click(object sender, EventArgs e)
		{
			ChartClear();
			ChartClear2();
			ChartClear3();
		}

		private void textBox1_TextChanged(object sender, EventArgs e)
		{

		}

		private void button3_Click(object sender, EventArgs e)
		{
			string Str = textBoxAvgParam.Text.Trim();
			double Num;
			bool isNum = double.TryParse(Str, out Num);

			if(string.IsNullOrWhiteSpace(Str) == true)
			{
				MessageBox.Show("Please insert numbers");
				return;
			}

			if (isNum)
				uartSendStr("avgcount " + Str + "\n");
			else
				MessageBox.Show("Please insert number only");

			LOG("FW Config average count set to " + textBoxAvgParam.Text + "\n");
		}

		private void button4_Click(object sender, EventArgs e)
		{
			SetOffset();
		}

		private void button4_Click_1(object sender, EventArgs e)
		{
			SetOffset();
		}

		private void button5_Click(object sender, EventArgs e)
		{
			// clear current offset
			uartSendStr("tu88_offset 0 0 0 0\n");
			Delay(1000);
		}

		private void button6_Click(object sender, EventArgs e)
		{
			uartSendStr("sens_th " + textBoxSensTh.Text + "\n");
		}
	}
}
