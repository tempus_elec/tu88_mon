﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TU88_Mon
{
	public partial class Form1 : Form
	{
		SerialPort sPort_Tempus1 = null;

		Mutex mtx = new Mutex();
		UInt64 counter = 0;
		UInt64 adcCounter = 0;
		UInt64 tempCounter = 0;

		Int32 tu88_0_value = 0;
		Int32 tu88_1_value = 0;
		Int32 tu88_2_value = 0;
		Int32 tu88_3_value = 0;

		bool ledFlag = false;

		void SerialPort_DataReceivedPackets(object sender, SerialDataReceivedEventArgs e)
		{
			try
			{
				mtx.WaitOne();

				const int LOG_SUB_OFFSET = 4;
				string strRecData;
				string[] spData;				
				int intRecSize = sPort_Tempus1.BytesToRead;
				byte[] buff;
				
				if (intRecSize != 0)
				{
					buff = new byte[intRecSize];

					try
					{
						strRecData = sPort_Tempus1.ReadLine();
						spData = strRecData.Split(' ');
						LOG(strRecData + "\n");

						if (spData[0] == "LOG")
						{
							//INFO(strRecData.Substring(LOG_SUB_OFFSET) + "\n");
						}
						else if (spData[0] == "ERR")
						{
							ERR(strRecData.Substring(LOG_SUB_OFFSET) + "\n");
						}
						else if (spData[0] == "MSG")
						{
							MsgParser(strRecData);
							INFO(strRecData.Substring(LOG_SUB_OFFSET) + "\n");
						}
						else if (spData[0] == "RAW")
						{

						}
						else
						{
							ERR("Received broken packet...\n\t: " + strRecData + "\n");
						}
					}
					catch (Exception ex)
					{
						LOG(ex.Message);
					}
				}
				else
				{

				}
				mtx.ReleaseMutex();
			}
			catch (System.IndexOutOfRangeException ex)  // CS0168
			{
				mtx.ReleaseMutex();
				ERR(CallerName() + " : " + ex.Message + '\n');
			}
		}

		public void MsgParser(string str)
		{
			string[] spData;

			spData = str.Split(' ');

			if(spData[1] == "ROIC")
			{
				chart_tu88_update(counter++, spData[2], spData[4], spData[6], spData[8]);
				chart_tu88_temp_update(tempCounter++, spData[3], spData[5], spData[7], spData[9]);
				CSV_SAVE(str);

				if(adjEvent != null)
				{
					tu88_0_value = Convert.ToInt32(spData[2]);
					tu88_1_value = Convert.ToInt32(spData[4]);
					tu88_2_value = Convert.ToInt32(spData[6]);
					tu88_3_value = Convert.ToInt32(spData[8]);
					adjEvent.Set();
				}
			}

			if (spData[1] == "AD")
			{
				chart_tu88_adc_update(adcCounter++, spData[2], spData[3], spData[4], spData[5]);
				CSV_SAVE(str);
			}

			if (spData[1] == "LED")
			{
				if( (spData[2] == "ON") && (ledFlag == false) )
				{
					LoadOnOffImage(true);
					ledFlag = true;
				}
				else if( (spData[2] == "OFF") && (ledFlag == true) )
				{
					LoadOnOffImage(false);
					ledFlag = false;
				}
			}
		}

		public void UartCom()
		{
			UInt16 isPortFound = 0;

			LOG("COM port finding...\n");
			buttonComOpen.Enabled = false;
			buttonComClose.Enabled = false;

			comboBoxCom.Items.Clear();
			comboBoxCom.BeginUpdate();

			foreach (string comport in SerialPort.GetPortNames())
			{
				comboBoxCom.Items.Add(comport);
				INFO("Found " + comport + "\n");

				isPortFound++;
			}
			comboBoxCom.EndUpdate();

			CheckForIllegalCrossThreadCalls = false;

			if (isPortFound != 0)
			{
				buttonComOpen.Enabled = true;
				buttonComClose.Enabled = true;
			}
			else
			{
				INFO("Couldn't found available COM port.\n");

				buttonComOpen.Enabled = false;
				buttonComClose.Enabled = false;
			}
		}

		private void UartOpen()
		{
			try
			{
				if (null == sPort_Tempus1)
				{
					if (comboBoxCom.SelectedItem == null)
					{
						LOG("시리얼 포트를 선택해 주세요!!!");
					}
					else
					{
						sPort_Tempus1 = new SerialPort();
						sPort_Tempus1.DataReceived += new SerialDataReceivedEventHandler(SerialPort_DataReceivedPackets);
						//sPort_Tempus.DataReceived += new SerialDataReceivedEventHandler(SerialPort_DataReceivedBytes);
						sPort_Tempus1.PortName = comboBoxCom.SelectedItem.ToString();
						sPort_Tempus1.BaudRate = 115200;     // 230400;
						sPort_Tempus1.DataBits = (int)8;
						sPort_Tempus1.Parity = Parity.None;
						sPort_Tempus1.StopBits = StopBits.One;
						sPort_Tempus1.ReadTimeout = -1;
						sPort_Tempus1.WriteTimeout = -1;
						sPort_Tempus1.NewLine = "\n";
						sPort_Tempus1.Open();
					}

				}

				if (sPort_Tempus1.IsOpen)
				{
					buttonComOpen.Enabled = false;
					buttonComClose.Enabled = true;
					LOG("시리얼 포트를 연결했습니다... : )\n");
				}
				else
				{
					if (comboBoxCom.SelectedItem != null)
						LOG("시리얼 포트가 사용중입니다\n");

					buttonComOpen.Enabled = false;
					buttonComClose.Enabled = true;
				}
			}
			catch (System.Exception ex)
			{
				ERR(CallerName() + " : " + ex.Message);
			}
		}

		private void UartClose()
		{
			try
			{
				if (null != sPort_Tempus1)
				{
					if (sPort_Tempus1.IsOpen)
					{
						sPort_Tempus1.Close();
						sPort_Tempus1.Dispose();
						sPort_Tempus1 = null;
						buttonComOpen.Enabled = true;
						buttonComClose.Enabled = false;
						LOG("시리얼 포트를 닫았습니다\n");
					}
					else
					{
						LOG("시리얼 포트가 닫혀있습니다\n");
					}
				}
			}
			catch (Exception ex)  // CS0168
			{
				ERR(CallerName() + " : " + ex.Message);
			}
		}

		private void UartSend(byte RegAddr, byte Reg1st, byte Reg2nd)
		{
			byte[] msg = { 0, 0, 0 };

			msg[0] = RegAddr;
			msg[1] = Reg1st;
			msg[2] = Reg2nd;

			if ((sPort_Tempus1 == null) || (sPort_Tempus1.IsOpen == false))
			{
				ERR("Please open COM port first!!!!\n");
			}
			else
			{
				sPort_Tempus1.Write(msg, 0, msg.Length);
				LOG("Send " + Str2Hex(msg[0].ToString()) + ", " + Str2Hex(msg[1].ToString()) + ", " + Str2Hex(msg[2].ToString()) + "\n");
			}
		}

		public void uartSendStr(string str)
		{
			byte[] msg = Encoding.UTF8.GetBytes(str);

			if ((sPort_Tempus1 == null) || (sPort_Tempus1.IsOpen == false))
			{
				ERR("시리얼 포트를 연결해 주세요\n");
				return;
			}

			for (int i = 0; i < msg.Length; i++)
			{
				sPort_Tempus1.Write(msg, i, 1);
				Delay(100);
			}
		}		
	}
}
